<?php
namespace model;

use PDO;

class CommentModel
{
    /** List all the comments of a specific product id
     * @param $productId Integer product id to show comments
     * @return array List of product's comments
     */
    public static function listComments(int $productId) : array {
        $db = Model::connect();

        // Requête SQL
        $sql = "SELECT comment.content, user.lastname AS lastname, user.firstname AS firstname, comment.date FROM comment 
        INNER JOIN user ON comment.id_user = user.id WHERE comment.id_product = :id ORDER BY comment.date DESC";

        $req = $db->prepare($sql);
        $req->execute([ "id" => $productId]);

        return $req->fetchAll();
    }

    /** Insert a comment into the database
     * @param int $userid The id of the user publishing the comment
     * @param int $productid The id of the product
     * @param string $message The content of the comment
     * @return bool True if comment has been added, False otherwise
     */
    public static function insertComment(int $userid, int $productid, string $message) : bool {
        if (strlen($message) < 2 || !isset($_SESSION['id'])) // check informations before adding
            return false;

        // add user to database
        $db = Model::connect();
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); // protect against injection

        // SQL request
        $sql = "INSERT INTO comment (comment.content, comment.date, comment.id_product, comment.id_user) VALUES
            (:content, NOW(), :productid, :userid)";

        $req = $db->prepare($sql);
        $req->execute([ 'content' => htmlspecialchars($message), 'productid' => $productid, 'userid' => $userid ]);

        return true;
    }
}