<?php
namespace model;

class StoreModel {

  static function listCategories(): array {
    // Connexion à la base de données
    $db = Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

    /** Makes an array containing products and their informations (can search through database if specified)
     * @param array|null $categories List of categories to search in (null if empty)
     * @param string $order Specify if user requested to order the results
     * @param string $nameFilter Filter the name of searched products (default="")
     * @return array Products from the store's database
     */
  static function listProducts(array $categories = null, string $order = "none", string $nameFilter = ""): array {
      $db = Model::connect();

      // sanitize categories array, if not empty
      if ($categories != null)
          for ($i = 0; $i < count($categories); ++$i)
              $categories[$i] = "'" . $categories[$i] . "'";

      // initialize filters with their default values
      $filter = "";
      $orderSql = " ORDER BY product.name ASC ";

      // check for filters, if specified
      // order filter
      if ($order === "asc")
          $orderSql = " ORDER BY product.price ASC ";
      else if ($order === "desc")
          $orderSql = " ORDER BY product.price DESC ";

      // name filter
      if ($nameFilter != "")
          $filter = " WHERE product.name LIKE :name ";

      // categories filter
      if ($categories != null){
          if ($filter === "")
              $filter = " WHERE category.name IN (".implode(',', $categories).") ";
          else
              $filter .= " AND category.name IN (".implode(',', $categories).") ";
      }

      // sql request
      $sql = "SELECT product.id, product.name, product.price, product.image, category.name AS category FROM product 
        INNER JOIN category ON product.category = category.id ".$filter.$orderSql;

      $req = $db->prepare($sql);

      // execute request
      if ($nameFilter != "")
          $req->execute([ 'name' => '%'.htmlspecialchars($nameFilter).'%']);
      else
          $req->execute();

      return $req->fetchAll();
  }

    /** Makes an array containing informations about a product
     * @param int $id Product id
     * @return array Product informations from the store's database
     */
    static function infoProduct(int $id): array {
        $db = Model::connect();

        // Requête SQL
        $sql = "SELECT product.id, product.name, product.price, product.image, product.image_alt1, product.image_alt2, 
            product.image_alt3, product.spec, product.developer, category.name AS category FROM product 
            INNER JOIN category ON product.category = category.id WHERE product.id = :id";

        $req = $db->prepare($sql);
        $req->execute([ "id" => htmlspecialchars($id)]);

        return $req->fetchAll();
    }
}
