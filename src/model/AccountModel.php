<?php
namespace model;
use PDO;

class AccountModel
{
    /** Check if the current sign-up informations are correct
     * @param $firstname String firstname of user
     * @param $lastname String lastname of user
     * @param $mail String mail address of user
     * @param $password String the non-encoded password of user
     * @return Boolean True if the informations are correct, false otherwise
     */
    private static function check(string $firstname, string $lastname, string $mail, string $password) : bool
    {
        return !(strlen($firstname) < 2 || strlen($lastname) < 2 // check firstname/lastname
            || !filter_var($mail, FILTER_VALIDATE_EMAIL) // check mail
            || strlen($password) < 6); // check password
    }

    /** Add the specified user informations to the database, if possible
     * @param $firstname String firstname of user
     * @param $lastname String lastname of user
     * @param $mail String mail address of user
     * @param $password String the non-encoded password of user
     * @return bool True if user has been added to the database, false otherwise, or a string for exceptions
     */
    public static function signin(string $firstname, string $lastname, string $mail, string $password) {
        if (!AccountModel::check($firstname, $lastname, $mail, $password)) // check informations before adding
            return false;

        // database connection
        $db = Model::connect();
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); // protect against injection

        // check if email already exists
        $sql_email="SELECT COUNT(*) as count FROM user WHERE user.mail LIKE :mail";
        $stmt = $db->prepare($sql_email);
        $stmt->execute([ "mail" => htmlspecialchars($mail) ]);
        $res = $stmt->fetch();

        if ($res['count'] > 0)
            return false;
        else{
            // add user to database
            // prepare data for insert
            $password = password_hash($password, PASSWORD_ARGON2I);

            // SQL request
            $sql = "INSERT INTO user (user.firstname, user.lastname, user.mail, user.password) VALUES
            (:firstname, :lastname, :mail, :password)";

            $req = $db->prepare($sql);
            $req->execute([ 'firstname' => $firstname, 'lastname' => $lastname, 'mail' => $mail, 'password' => $password ]);
            return true;
        }
    }

    /** Get a specified user in the database from its mail/password
     * @param string $mail
     * @return array The current selected user, if any
     */
    public static function login(string $mail) {
        $db = Model::connect();

        // SQL request
        $sql = "SELECT user.id, user.lastname, user.firstname, user.mail, user.password 
                FROM user WHERE user.mail=:mail LIMIT 1"; // don't check the hashed password here

        $req = $db->prepare($sql);
        $req->execute([ 'mail' => $mail ]);

        return $req->fetch();
    }

    /** Load required informations from a specific user
     * @param int $id User's id
     * @return mixed An array containing user's informations
     */
    public static function userInfo(int $id) {
        $db = Model::connect();

        // SQL request
        $sql = "SELECT user.id, user.firstname, user.lastname, user.mail, user.password 
                FROM user WHERE user.id=:id LIMIT 1";

        $req = $db->prepare($sql);
        $req->execute([ 'id' => $id ]);

        return $req->fetch();
    }

    /** Update user's informations in the database
     * @param int $id the user's id
     * @param string $firstname the new firstname of the user
     * @param string $lastname the new lastname of the user
     * @param string $mail the new mail of the user
     * @param string $password the new password of the user
     * @return bool True if informations have been updated, False otherwise
     */
    public static function update(int $id, string $firstname, string $lastname, string $mail, string $password) {
        $db = Model::connect();

        // SQL request
        $nbUpdates = 0;
        $sql_params = [];
        $sql = "UPDATE user SET ";

        // if user updated firstname
        if (strlen($firstname) > 0){
            if (strlen($firstname) > 2) {
                $sql .= " user.firstname = :firstname";
                $sql_params['firstname'] = htmlspecialchars($firstname);
                $nbUpdates++;
            } else
                return false;
        }

        // if user updated lastname
        if (strlen($lastname) > 0){
            if (strlen($lastname) > 2) {
                $sql .= ($nbUpdates > 0 ? ", " : "")." user.lastname = :lastname";
                $sql_params['lastname'] = htmlspecialchars($lastname);
                $nbUpdates++;
            } else
                return false;
        }

        // if user updated mail
        if (strlen($mail) > 0){

            // check if mail is already in use
            $sql_email="SELECT COUNT(*) as count FROM user WHERE user.mail LIKE :mail";
            $stmt = $db->prepare($sql_email);
            $stmt->execute([ "mail" => htmlspecialchars($mail) ]);
            $res = $stmt->fetch();

            if ($res['count'] > 0)
                return false;

            if (strlen($mail) > 2) {
                $sql .= ($nbUpdates > 0 ? ", " : "") . " user.mail = :mail";
                $sql_params['mail'] = htmlspecialchars($mail);
                $nbUpdates++;
            }else
                return false;
        }

        // if user updated password
        if (strlen($password) > 0){
            if (strlen($password) > 5) {
                // prepare data for insert
                $password = password_hash($password, PASSWORD_ARGON2I);

                $sql .= ($nbUpdates > 0 ? ", " : "")." user.password = :password";
                $sql_params['password'] = $password;
            } else
                return false;
        }

        $sql .= " WHERE user.id = :id";
        $sql_params['id'] = htmlspecialchars($id);

        // update user in database
        $req = $db->prepare($sql);
        $req->execute($sql_params);

        // update session name variable
        $_SESSION['name'] = (array_key_exists("firstname", $sql_params)
                ? $firstname : explode(" ", $_SESSION['name'])[0])
            ." ".(array_key_exists("lastname", $sql_params)
                ? $lastname : explode(" ", $_SESSION['name'])[1]);

        return true;
    }
}