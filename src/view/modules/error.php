<div id="error-page" class="animate__animated animate__fadeInUp" style="width: 80%; padding-top: 5%; padding-bottom: 5%; padding-left: 10%">
    <h1>Oups...</h1>

    <p>On dirait que la page demandée n'existe pas !</p>

    <a class="simple-button" href="/" style="margin-top: 20px">Retour à l'accueil</a>
</div>