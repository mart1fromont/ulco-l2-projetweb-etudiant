<?php
// RSS Feed URL
$url = "https://store.steampowered.com/feeds";

if(@simplexml_load_file($url))
    $feeds = simplexml_load_file($url);
else
    $invalidurl = true;

$i=0;
 ?>

<div id="news">

    <h1 class="animate__animated animate__fadeInUp" style="margin-bottom: 50px">Dernières actualités de Staem</h1>

    <div class="products animate__animated animate__fadeInUp">
        <?php if (isset($invalidurl) && $invalidurl == true) : ?>
            <div id="error" class="animate__animated animate__fadeInUp" style="max-width: 50%; max-height: 60%;
        margin: 20px 5px 30px 25px;">
                <h2 style="font-size: 18px">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white"
                         class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                             0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                    </svg>
                    Une erreur est survenue
                </h2>
                <p style="font-size: small">
                    Le flux d'actualités de Staem n'est pas disponible pour le moment.
                </p>
            </div>
        <?php endif ?>

        <?php  if(!empty($feeds)){ foreach ($feeds->item as $item) {

            $title =  str_replace("Steam", "Staem", $item->title);
            $link = $item->link;
            $category = str_replace("Valve", "Staem", $item->category);
            $description =  str_replace("Steam", "Staem", $item->encoded);
            $e_content     = $item->children("content", true);
            $e_encoded     = (string)$e_content->encoded;
            $e_encoded = str_replace("Steam", "Staem", $e_encoded);
            $postDate = $item->pubDate;
            $pubDate = date('D, d M Y',strtotime($postDate));

            ?>
            <div class="news-item">
                <div class="news-item-head">
                    <h2><a class="news-item-title" href="<?= $link ?>"><?= $title ?></a></h2>
                    <h3 class="news-item-category"><?= $category ?>, <span><?php echo $pubDate; ?></span></h3>
                </div>
                <div class="news-item-content">
                    <?= implode(' ', array_slice(explode(' ', $e_encoded), 0, 55))
                    ."..."; ?><br><a href="<?= $link ?>" class="news-item-content-link">En savoir plus</a>
                </div>
            </div>
        <?php ++$i; if($i>=10) break; }} ?>

        <a href="https://store.steampowered.com/news/collection/steam/">
            <input type="button" class="simple-button" value="En voir plus sur le blog Staem"/>
        </a>
    </div>
</div>