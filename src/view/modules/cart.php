<?php use model\StoreModel;
if (isset($_SESSION['cart'])) $userCart = $_SESSION['cart']; ?>

<style>body {background: rgba(31,33,37,1) linear-gradient(157deg, rgba(33,36,41,1) 35%, rgba(31,33,37,1) 100%);}</style>

<div id="account-infos" class="animate__animated animate__fadeInUp">


    <div class="info-box animate__animated animate__fadeInUp">
        <h1>Mon panier</h1>
        <div class="tab">
            <a href="/account/infos">
                <button class="tablinks" onclick="openTab(event, 'tab-general')" id="defaultOpen">
                    Mon compte
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         class="bi bi-box-arrow-in-up-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M6.364 13.5a.5.5 0 0 0 .5.5H13.5a1.5 1.5 0 0 0 1.5-1.5v-10A1.5 1.5
                        0 0 0 13.5 1h-10A1.5 1.5 0 0 0 2 2.5v6.636a.5.5 0 1 0 1 0V2.5a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1
                        .5.5v10a.5.5 0 0 1-.5.5H6.864a.5.5 0 0 0-.5.5z"/>
                        <path fill-rule="evenodd" d="M11 5.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793l-8.147 8.146a.5.5
                        0 0 0 .708.708L10 6.707V10.5a.5.5 0 0 0 1 0v-5z"/>
                    </svg>
                </button>
            </a>
        </div>
    </div>

    <div id="tab-general" class="tabcontent animate__animated animate__fadeInUp active" style="display: block">

        <?php if (isset($params['add']) && $params['add'] == "success") : ?>
            <div id="valid" class="info-pane animate__animated animate__fadeInUp" style="margin-left: 25px !important;">
                <h2 style="text-transform: initial">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white"
                         class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                    </svg>
                    Le produit a bien été modifié
                </h2>
                <p>Votre panier a correctement été modifié.</p>
            </div>
        <?php elseif (isset($params['add']) && $params['add'] == "drop") : ?>
            <div id="error" class="info-pane animate__animated animate__fadeInUp" style="margin-left: 25px !important;">
                <h2 style="text-transform: initial">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white"
                         class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                    </svg>
                    Le produit a bien été supprimé
                </h2>
                <p>Votre panier a correctement été modifié.</p>
            </div>
        <?php endif; ?>

        <div>

            <?php if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0): ?>
                <div class="animate__animated animate__fadeInUp" style="margin-left: 20px">
                    <h2><?= count($_SESSION['cart']) ?> Produit<?= (count($_SESSION['cart']) > 1 ? "s" : "") ?></h2>
                </div>
            <?php endif; ?>

            <?php $total = 0; if (isset($_SESSION['cart'])) foreach ($userCart as $product) { // for each product in cart
                $productInfos = StoreModel::infoProduct($product['id'])[0];
                $total += $productInfos['price'] * $product['count']; ?>

                <div class="cart-product animate__animated animate__fadeInUp">
                    <div style="width: 150px">
                        <img height=200 src="/public/images/<?= $productInfos['image'] ?>" alt="<?= $productInfos['name'] ?>">
                    </div>

                    <div class="cart-product-name">
                        <p class="product-category"><?= $productInfos["category"] ?></p>
                        <a href="/store/<?= $productInfos['id'] ?>">
                            <h5 style="font-size: xx-large"><?= $productInfos['name'] ?></h5>
                        </a>
                        <h3 class="developer">de <?= $productInfos["developer"] ?></h3>
                    </div>

                    <form id="form-<?= $productInfos['id'] ?>" method="post" action="/cart/add">
                        <div class="cart-product-quantity">
                            <h5 style="font-size: large">Quantité</h5>
                            <div style="display: flex; max-height: 50px">
                                <button class="simple-button" type="button"
                                        onclick="changeProductCount(<?= $productInfos['id'] ?>, -1)">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="whitesmoke"
                                         class="bi bi-dash" viewBox="0 0 16 16">
                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                    </svg>
                                </button>
                                <input type="hidden" name="name" value="<?= $productInfos['id'] ?>">
                                <input type="hidden" id="product-select-count-input-<?= $productInfos['id'] ?>"
                                       name="count" value=<?= $product['count'] ?>>
                                <button id="product-select-count-<?= $productInfos['id'] ?>" class="count-button" disabled
                                        value="<?= $product['count'] ?>"> <?= $product['count'] ?> </button>
                                <button class="simple-button" type="button"
                                        onclick="changeProductCount(<?= $productInfos['id'] ?>, 1)">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="whitesmoke"
                                         class="bi bi-plus" viewBox="0 0 16 16">
                                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0
                                    0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="cart-product-quantity">
                        <h5 style="font-size: large">Prix unitaire</h5>
                        <h2 style="font-size: xx-large; padding-top: 10px"><?= $productInfos['price'] ?>€</h2>
                    </div>
                </div>

            <?php } ?>

            <?php if ($total > 0) : ?>
                <div class="cart-total">
                    <h3>Sous-total: <?= $total ?>€</h3>
                    <h3>Livraison*: <?= ($total >= 50 ? "Offerte" : "4.99€") ?></h3>

                    <h2>Total :
                        <span style="font-size: xxx-large"><?= ($total >= 50 ? $total : $total + 4.99) ?>€</span>
                    </h2>
                    <input class="simple-button big-button" id="product-pay" type="submit"
                           value="Procéder au paiement" style="margin-top: 15px">
                    <p class="condition-text">*La livraison est gratuite pour les commandes supérieures à 50€ TTC</p>
                </div>
            <?php else : ?>
                <div id="info-commands-none" class="box warning" style="margin-left: 30px">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         class="bi bi-bag-check-fill" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2
                2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zm-.646 5.354a.5.5 0 0 0-.708-.708L7.5 10.793
                6.354 9.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
                    </svg>
                    Le panier est vide. <a href="/store">Ajouter un article ?</a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script src="/public/scripts/cart.js"></script>
