<?php $user = $params['user']; ?>

<style>body {background: rgba(31,33,37,1) linear-gradient(157deg, rgba(33,36,41,1) 35%, rgba(31,33,37,1) 100%);}</style>

<div id="account-infos" class="animate__animated animate__fadeInUp">


    <div class="info-box animate__animated animate__fadeInUp">
        <h1>Informations du compte</h1>
        <div class="tab">
            <button class="tablinks" onclick="openTab(event, 'tab-general')" id="defaultOpen">Général</button>
            <button class="tablinks" onclick="openTab(event, 'tab-password')">Mot de passe</button>
            <button class="tablinks" onclick="openTab(event, 'tab-commands')">Commandes</button>
            <a href="/account/cart">
                <button class="tablinks">
                    Panier
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-box-arrow-in-up-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M6.364 13.5a.5.5 0 0 0 .5.5H13.5a1.5 1.5 0 0 0 1.5-1.5v-10A1.5 1.5
                        0 0 0 13.5 1h-10A1.5 1.5 0 0 0 2 2.5v6.636a.5.5 0 1 0 1 0V2.5a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1
                        .5.5v10a.5.5 0 0 1-.5.5H6.864a.5.5 0 0 0-.5.5z"/>
                        <path fill-rule="evenodd" d="M11 5.5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793l-8.147 8.146a.5.5
                        0 0 0 .708.708L10 6.707V10.5a.5.5 0 0 0 1 0v-5z"/>
                    </svg>
                </button>
            </a>
        </div>
    </div>


    <div id="tab-general" class="tabcontent animate__animated animate__fadeInUp">

        <?php if (isset($params['status']) && $params['status'] == "update_failed") : ?>
            <div id="error" class="info-pane animate__animated animate__fadeInUp" style="max-width: 100%">
                <h2 style="text-transform: initial">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white"
                         class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                    </svg>
                    Une erreur est survenue
                </h2>
                <p>Un problème a empêché la mise à jour de vos données.<br>Vérifiez les informations et réessayez.</p>
            </div>
        <?php elseif (isset($params['status']) && $params['status'] == "update_success") : ?>
            <div id="valid" class="info-pane animate__animated animate__fadeInUp" style="max-width: 100%">
                <h2 style="text-transform: initial">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                         class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
                    7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                    </svg>
                    Informations mises à jour
                </h2>
                <p>Les informations de votre compte Staem ont bien été enregistrées.</p>
            </div>
        <?php elseif (isset($params['status']) && $params['status'] == "stw_rqst") : ?>
            <div id="valid" class="info-pane animate__animated animate__fadeInUp" style="width: 100%; max-width: 100%">
                <h2 style="text-transform: initial">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                         class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
                    7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                    </svg>
                    Demande envoyée
                </h2>
                <p>Votre demande d'accès à Staemworks a bien été prise en compte.</p>
            </div>
        <?php elseif (isset($params['status']) && $params['status'] == "no_stw_rqst") : ?>
            <div id="error" class="info-pane animate__animated animate__fadeInUp" style="width: 100%; max-width: 100%">
                <h2 style="text-transform: initial">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white"
                         class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                        0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
                    </svg>
                    Demande non-envoyée
                </h2>
                <p>Le domaine (<?= $_SERVER['SERVER_NAME'] ?>) n'est pas valide.</p>
            </div>
        <?php endif; ?>

        <h2>général</h2>
        <p>Modifier les informations de base de votre compte Staem.<br>
            <span style="font-style: italic; font-size: medium">Laisser vide pour ne pas modifier les champs</span>
        </p>

        <form id ="info-form" method="post" action="/account/update">
            <p id="info-form-firstname-label">Prénom</p>
            <input type="text" id="info-form-firstname" name="firstname" placeholder="<?= $user['firstname'] ?>" />

            <p id="info-form-lastname-label">Nom</p>
            <input type="text" id="info-form-lastname" name="lastname" placeholder="<?= $user['lastname'] ?>" />

            <p id="info-form-mail-label">Adresse mail</p>
            <input type="text" id="info-form-mail" name="mail" placeholder="<?= $user['mail'] ?>" />

            <br><input class="simple-button" type="submit" value="Modifier les informations" />
        </form>

        <h2 style="margin-top: 40px">Staemworks</h2>
        <p>Staemworks est un ensemble d'outils et de services destiné à aider les équipes de <br>développement et
            d'édition à développer leurs jeux et à tirer le meilleur<br>parti de leur distribution sur Staem.
        </p>
        <h5><br>Envoyez-nous une demande et nous vous répondrons rapidement pour vous<br>indiquer les prochaines étapes
            avant de publier votre jeu sur Staem !</h5>

        <br><a href="/account/staemworks-request">
            <input class="simple-button" type="submit" value="Envoyer une demande" />
        </a>
    </div>

    <div id="tab-password" class="tabcontent animate__animated animate__fadeInUp">
        <h2>mot de passe</h2>
        <p>Modifier le mot de passe de votre compte Staem.<br>Le mot de passe doit contenir au moins 5 caractères,
            dont 1 chiffre et 1 lettre.<br>
            <span style="font-style: italic; font-size: medium">Laisser vide pour ne pas modifier les champs</span>
        </p>

        <form id ="password-form" method="post" action="/account/update">
            <p id="password-form-password1-label">Nouveau mot de passe</p>
            <input type="password" id="password-form-password1" name="password1" required />

            <p id="password-form-password2-label">Ressaisir le nouveau mot de passe</p>
            <input type="password" id="password-form-password2" name="password2" required />

            <br><input class="simple-button" type="submit" value="Modifier le mot de passe" />
        </form>
    </div>

    <div id="tab-commands" class="tabcontent animate__animated animate__fadeInUp">
        <h2>commandes</h2>
        <div id="info-commands-none" class="box warning">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                 class="bi bi-bag-check-fill" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2
                2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zm-.646 5.354a.5.5 0 0 0-.708-.708L7.5 10.793
                6.354 9.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
            </svg>
            Vous n'avez aucune commande en cours
        </div>
    </div>

</div>

<script src="/public/scripts/account-tab.js"></script>