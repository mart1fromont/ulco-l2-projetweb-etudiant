<div id="product-infos-small" class="product-infos-small hidden">
    <h2 style="padding-left: 15px">The Arcade Tower </h2>
    <h3 class="developer-small">de Baptiste Duquenne et Martin Fromont</h3>
    <div style="margin-left: auto; display: flex">
        <p class="product-price-small">Sortie en 2021</p>
        <input disabled class="simple-button" id="product-add-to-cart-small" type="submit" value="Précommander"
               onclick="window.scrollTo({ top: 0, behavior: `smooth` })">
    </div>
</div>

<div id="product" class="animate__animated animate__fadeInUp">
    <div>

        <div class="product-images">
            <img id="product-showcase-main" src="/public/images/new/tat_1.png" alt="The Arcade Tower">

            <div class="product-miniatures">
                <div>
                    <img class="product-image" src="/public/images/new/tat_1.png" alt="The Arcade Tower 1">
                </div>
                <div>
                    <img class="product-image" src="/public/images/new/tat_2.png" alt="The Arcade Tower 2">
                </div>
                <div>
                    <img class="product-image" src="/public/images/new/tat_3.png" alt="The Arcade Tower 3">
                </div>
            </div>
        </div>


        <div class="product-infos">
            <div class="product-infos-div">
                <p class="product-category">Aventure</p>
                <h1>The Arcade Tower</h1>
                <h3 class="developer">de Baptiste Duquenne et Martin Fromont</h3>
                <p class="product-price">Sortie en 2021</p>

                <?php if (isset($_SESSION['id'])) : ?>
                    <form method="post" action="/cart/add">
                <?php else : ?>
                    <form>
                <?php endif; ?>
                    <div style="display: flex; max-height: 30px">
                        <br>
                        <div id="product-select-error" class="box error">
                            Quantité maximale autorisée !
                        </div>
                    </div>
                        <input disabled class="simple-button" id="product-add-to-cart" type="submit"
                               value="La précommande n'est pas encore disponible">
                </form>
            </div>

        </div>
    </div>

    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            Les développeurs de The Arcade Tower n'ont pas encore publié d'informations sur leur produit.
        </div>
    </div>
</div>

<script src="/public/scripts/product.js"></script>