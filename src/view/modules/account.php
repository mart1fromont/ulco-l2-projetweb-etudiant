<style>body {background: rgba(31,33,37,1) linear-gradient(157deg, rgba(33,36,41,1) 35%, rgba(31,33,37,1) 100%);}</style>

<?php if (isset($params['status']) && $params['status'] == "login_failed") : ?>
    <div id="error" class="animate__animated animate__fadeInUp" style="margin-left: 150px">
        <h2>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white"
                 class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
            </svg>
            Une erreur est survenue
        </h2>
        <p>La connexion a échoué. Vérifiez vos identifiants et réessayez</p>
    </div>
<?php elseif (isset($params['status']) && $params['status'] == "signin_success") : ?>
    <div id="valid" class="animate__animated animate__fadeInUp" style="margin-left: 150px">
        <h2>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                 class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
                    7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </svg>
            Inscription réussie !
        </h2>
        <p>Vous pouvez dès à présent vous connecter</p>
    </div>
<?php elseif (isset($params['status']) && $params['status'] == "signin_failed") : ?>
    <div id="error" class="animate__animated animate__fadeInUp" style="margin-left: 150px">
        <h2>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white"
                 class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
            </svg>
            Une erreur est survenue
        </h2>
        <p>L'inscription a échoué. Vérifiez vos identifiants et réessayez.</p>
    </div>
<?php elseif (isset($params['status']) && $params['status'] == "logout") : ?>
    <div id="valid" class="animate__animated animate__fadeInUp" style="margin-left: 150px">
        <h2>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                 class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
                    7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </svg>
            Vous avez été déconnecté
        </h2>
        <p>Vous pouvez vous reconnecter avec vos identifiants</p>
    </div>
<?php endif ?>

<div id="account" class="animate__animated animate__fadeInUp">

    <div style="display: flex">
        <form class="account-login animate__animated animate__fadeInUp" method="post" action="/account/login">

          <h2>Connexion</h2>

          <p>Adresse mail du compte Staem</p>
          <input type="text" name="mail" placeholder="Adresse mail" required/>

          <p>Mot de passe</p>
          <input type="password" name="password" placeholder="Mot de passe" required/>

          <br><input class="simple-button" type="submit" value="Connexion" />

        </form>
    </div>

    <form id="signin-form" class="account-signin" method="post" action="/account/signin">

      <h2>Inscription</h2>
      <h3>Créez votre compte Staem rapidement en remplissant le formulaire ci-dessous.</h3>

      <p id="lastname-label">Nom</p>
      <input type="text" name="lastname" id="lastname" placeholder="Nom" required/>

      <p id="firstname-label">Prénom</p>
      <input type="text" name="firstname" id="firstname" placeholder="Prénom" required/>

      <p id="mail-label">Adresse mail</p>
      <input type="text" name="mail" id="mail" placeholder="Adresse mail" required/>

      <p id="password1-label">Mot de passe</p>
      <input type="password" name="password" id="password1" placeholder="Mot de passe" required/>

      <p id="password2-label">Répéter le mot de passe</p>
      <input type="password" name="password" id="password2" placeholder="Mot de passe" required/>

      <br><input class="simple-button" id="submit" type="submit" value="Inscription" />

    </form>
</div>

<script src="/public/scripts/signin.js"></script>