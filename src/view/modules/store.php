<div id="store">

    <!-- Affichage des produits --------------------------------------------------->

    <div class="products animate__animated animate__fadeInUp">
        <?php if (isset($params['status']) && $params['status'] == "login_success") : ?>
            <div id="valid" class="animate__animated animate__fadeInUp" style="min-width: 90%; max-height: 60%;
        margin: -20px 5px 30px 25px;">
                <h2 style="font-size: 18px">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
                    7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                    </svg>
                    Vous êtes connecté à votre compte Staem
                </h2>
                <p style="font-size: small">
                    Vous pouvez enregistrer vos achats, et modifier vos informations en cliquant sur votre nom
                </p>
            </div>
        <?php endif ?>

        <p class="search-count"><?= count($params["products"]) ?>
            <?= (count($params['products']) > 1 ? "résultats correspondent " : "résultat correspond ") ?>
            à votre recherche</p>

        <?php if (count($params['products']) < 1) : ?>
            <p style="margin-left: 35px; margin-top: 25px">Aucun résultat ne correspond à votre requête.</p>
        <?php endif; ?>

        <?php foreach ($params["products"] as $p) { ?>
            <div class="card">

                <div style="width: 100px">
                    <p class="card-image">
                        <img src="/public/images/<?= $p["image"] ?>" alt="<?= $p["name"] ?>">
                    </p>
                </div>

                <div style="margin-left: 25px; margin-top: 3px; width: 400px">
                    <p class="card-category"><?= $p["category"] ?></p>

                    <p class="card-title">
                        <a href="/store/<?= $p["id"] ?>"><?= $p["name"] ?></a>
                    </p>
                </div>

                <div>
                    <p class="card-price"><?= $p["price"] ?> €</p>
                </div>

            </div>
        <?php } ?>
    </div>

<!-- Filtrer l'affichage des produits  ---------------------------------------->

<form method="post" action="/store/search" class="animate__animated animate__fadeInUp">

    <div class="search-bloc">
        <div class="search-bloc-header">
            <h4> Affiner par nom</h4>
        </div>
        <div class="search-bloc-content">
            <input type="text" name="search" id="search" placeholder="Rechercher un produit"
                   value="<?= ($_GET['search'] ?? "") ?>"/>
        </div>
    </div>

    <div class="search-bloc">
        <div class="search-bloc-header">
            <h4> Affiner par tag</h4>
        </div>
        <div class="search-bloc-content">
            <?php foreach ($params["categories"] as $c) { ?>
                <input type="checkbox" class="category-checkbox" name="category[]" value="<?= $c["name"] ?>" />
                <?= $c["name"] ?>
                <br/>
            <?php } ?>
        </div>
    </div>

    <div class="search-bloc">
        <div class="search-bloc-header">
            <h4> Affiner par prix</h4>
        </div>
        <div class="search-bloc-content">
            <input type="radio" name="order" id="order-asc-checkbox" value="asc"
                <?php if(isset($_GET['order']) && $_GET['order'] === "asc") echo ' checked="true" '; ?>/> Croissant <br />
            <input type="radio" name="order" id="order-desc-checkbox" value="desc"
                <?php if(isset($_GET['order']) && $_GET['order'] === "desc") echo ' checked="true" '; ?>/> Décroissant <br />
            <input type="radio" name="order" id="order-none-checkbox" value="none"/> Peu importe <br />
        </div>
    </div>

  <div>
      <input class="simple-button" type="submit" value="Appliquer" />
      <input class="simple-button simple-button-reset" type="reset" value="Réinitialiser" onclick="resetForm(this)"/>
  </div>

</form>

</div>

<script src="/public/scripts/store-search.js"></script>
