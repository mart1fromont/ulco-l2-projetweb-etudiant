<div id="home">

    <div class="banner banner-home" style="color: white">
        <div style="width: 90%; padding-left: 5%">
            <h1 class="animate__animated animate__fadeInUp">Jouez sans limite</h1>

            <h2 class="animate__animated animate__fadeInUp">Découvrez les dernier jeux PC disponibles, dès
                maintenant sur Staem<br></h2>

            <p class="animate__animated animate__fadeInUp">
                <a class="simple-button" href="/store">Visiter la boutique</a>
            </p>
        </div>
    </div>

    <div class="brief animate__animated animate__fadeInUp">
        <div class="brief-image animate__animated animate__fadeInUp">
            <img src="/public/images/new/home_1.png" alt="Home image 1">
        </div>

        <div class="brief-text animate__animated animate__fadeInUp" style="margin-right: 10%">
            <h3>Boutique en ligne</h3>

            <p>
                Ne passez pas à côté des derniers jeux PC, et complètez votre collection
                dès maintenant en visitant la boutique en ligne. Ne manquez pas les bonnes
                affaires et les promotions à 90% tout au long de l'année sur certains jeux !
                La livraison est gratuite pour toute commande de plus de 50€.<br>
            </p>

            <p>
                <br><a class="simple-button" href="/store">Parcourir le catalogue</a>
            </p>
        </div>
    </div>

    <div class="brief animate__animated animate__fadeInUp" style="width: 80%; padding-left: 10%">
        <div class="brief-text animate__animated animate__fadeInUp" style="text-align: right">
            <h3>Réseaux sociaux</h3>

            <p>
                Enregistre tes meilleures parties ou met en scène tes constructions au travers de
                vidéos, puis partage ton expérience avec le monde entier en rejoignant la
                communauté Staem sur les réseaux sociaux.<br>
            </p>

            <p>
                <br><a class="simple-button-reset" href="https://www.facebook.com/Steam">Facebook</a>
                 <a class="simple-button-reset" href="https://www.instagram.com/steam_games_official/?hl=fr">Instagram</a>
                 <a class="simple-button-reset" href="https://www.youtube.com/channel/UCg0FSqPeiGD_lIiPaaAehQg">YouTube</a>
            </p>
        </div>

        <div class="brief-image animate__animated animate__fadeInUp">
            <img src="/public/images/new/home_2.jpg" alt="Home image 2">
        </div>
    </div>
</div>