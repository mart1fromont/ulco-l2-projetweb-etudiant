<?php $info = $params["product"];
      setlocale(LC_TIME, 'fr_FR.utf8','fra'); // will be used to show locale date for comments
?>

<?php if (isset($params['status']) && $params['status'] == "post_success") : ?>
    <div id="valid" class="animate__animated animate__fadeInUp">
        <h2>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
                 class="bi bi-check-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384
                    7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </svg>
            Votre commentaire a été publié
        </h2>
        <p>Votre commentaire est disponible à la vue de tous. Voyez par vous-même !</p>
    </div>
    <br> <br>
<?php elseif (isset($params['status']) && $params['status'] == "post_failed") : ?>
    <div id="error" class="animate__animated animate__fadeInUp">
        <h2>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="white"
                 class="bi bi-exclamation-circle-fill" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0
                0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/>
            </svg>
            Votre commentaire n'a pas été publié
        </h2>
        <p>Vérifiez que vous êtes connecté et que votre message fait au moins 2 caractères.</p>
    </div>
    <br> <br>
<?php endif; ?>

<div id="product-infos-small" class="product-infos-small hidden">
    <h2 style="padding-left: 15px"> Acheter <?= $info["name"] ?> </h2>
    <h3 class="developer-small">de <?= $info["developer"] ?></h3>
    <div style="margin-left: auto; display: flex">
        <p class="product-price-small"><?= $info["price"] ?> € </p>
        <input class="simple-button" id="product-add-to-cart-small" type="submit" value="Ajouter au panier"
               onclick="window.scrollTo({ top: 0, behavior: `smooth` })">
    </div>
</div>

<div id="product" class="animate__animated animate__fadeInUp">
    <div>

        <div class="product-images">
            <img id="product-showcase-main" src="/public/images/<?= $info["image"] ?>" alt="<?= $info["name"] ?>">

            <div class="product-miniatures">
                <div>
                    <img class="product-image" src="/public/images/<?= $info["image"] ?>" alt="<?= $info["name"] ?> 1">
                </div>
                <div>
                    <img class="product-image" src="/public/images/<?= $info["image_alt1"] ?>" alt="<?= $info["name"] ?> 2">
                </div>
                <div>
                    <img class="product-image" src="/public/images/<?= $info["image_alt2"] ?>" alt="<?= $info["name"] ?> 3">
                </div>
                <div>
                    <img class="product-image" src="/public/images/<?= $info["image_alt3"] ?>" alt="<?= $info["name"] ?> 4">
                </div>
            </div>
        </div>


        <div class="product-infos">
            <div class="product-infos-div">
                <p class="product-category"><?= $info["category"] ?></p>
                <h1><?= $info["name"] ?></h1>
                <h3 class="developer">de <?= $info["developer"] ?></h3>
                <p class="product-price"><?= $info["price"] ?> €</p>

                <?php if (isset($_SESSION['id'])) : ?>
                    <form method="post" action="/cart/add">
                <?php else : ?>
                    <form>
                <?php endif; ?>
                    <div style="display: flex; max-height: 30px">
                        <button id="product-select-remove" class="simple-button" type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="whitesmoke"
                                 class="bi bi-dash" viewBox="0 0 16 16">
                                <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                            </svg>
                        </button>
                        <input type="hidden" name="name" value="<?= $info['id'] ?>">
                        <input type="hidden" id="product-select-count-input" name="count" value="1">
                        <button id="product-select-count" type="button" value="1">1</button>
                        <button id="product-select-add" class="simple-button" type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="whitesmoke"
                                 class="bi bi-plus" viewBox="0 0 16 16">
                                <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0
                                0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                            </svg>
                        </button>
                        <div id="product-select-error" class="box error">
                            Quantité maximale autorisée !
                        </div>
                    </div>
                    <?php if (isset($_SESSION['id'])) : ?>
                        <input class="simple-button" id="product-add-to-cart" type="submit" value="Ajouter au panier">
                    <?php else : ?>
                        <a href="/account"><input class="simple-button" id="product-add-to-cart" type="button"
                               value="Connectez vous pour modifier votre panier" style="cursor: pointer;"></a>
                    <?php endif; ?>
                </form>
            </div>

        </div>
    </div>

    <div>
        <div class="product-spec">
            <h2>à propos de ce jeu</h2>
            <?= $info["spec"] ?>
        </div>
    </div>
    <div class="product-comments" style="display: block">
        <h3 style="background-image: none">
            <?php if (count($params['comments']) > 0) echo count($params['comments']); ?>
            Évaluation<?php if (count($params['comments']) >= 2 || count($params['comments']) == 0) echo "s"; ?>
        </h3>

        <?php foreach ($params["comments"] as $c) { ?>
            <div class="product-comment">
                <p class="product-comment-author">
                    <?= $c['firstname'] ?> <?= $c['lastname'] ?>
                    <span>évaluation publiée le <?= strftime('%d %B %G', strtotime($c['date'])) ?></span>
                </p>
                <p>
                    <?= $c['content'] ?>
                </p>
            </div>
        <?php } ?>

        <?php if (count($params['comments']) <= 0) : ?>
            <div class="product-comment">
                <p class="product-comment-author">Il n'y a pas encore d'évaluation</p>
                <h5 style="margin-bottom: -5px">
                    Soyez le premier à donner votre avis !
                </h5>
            </div>
        <?php endif; ?>

        <?php if (isset($_SESSION['id'])) : ?>
            <form class="account-login animate__animated animate__fadeInUp" method="post"
                  action="/store/postComment" style="display: block">
                <h3>Donnez votre avis sur <?= $info['name'] ?></h3>
                <p><i>Note: votre avis doit contenir au minimum 2 caractères</i></p>
                <input type="hidden" name="product-id" value="<?= $info["id"]; ?>">
                <input type="text" id="new-comment-text" name="new-comment-text" placeholder="Rédigez une évaluation"
                style="max-width: 60%; height: 70px; vertical-align: text-top">
                <div><input class="simple-button" type="submit" value="Publier" /></div>
            </form>
        <?php else : ?>
            <p style="margin-top: 30px">Vous n'êtes pas connecté. <a href="/account" style="text-decoration: underline">Connectez-vous</a>
                pour rédiger un commentaire</p>
        <?php endif; ?>

    </div>
</div>

<script src="/public/scripts/product.js"></script>