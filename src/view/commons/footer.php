<footer class="animate__animated animate__fadeInUp">
    <img width="200" src="/public/images/new/logo.png" alt="Staem logo" style="margin-right: -30px;">
    <p><strong>Projet Web, fièrement propulsé par
            <a href="https://cloud-isnvie.alwaysdata.net/">
                <img alt="verifix-logo" height="15" src="https://isnvie.alwaysdata.net/img/logo-new-standard-small.png"
            </a>
        </strong>
        <br>
    </p>
    <p>Martin Fromont</p>
    <p><span style="font-size: smaller">L2 Informatique - 2021</span></p>

    <p style="font-size: x-small; font-weight: lighter"><br>© 2021 Staem Corporation. Tous droits réservés. Toutes les marques commerciales
        sont la propriété de leurs détenteurs respectifs, aux États-Unis et dans d'autres pays.</p>
</footer>