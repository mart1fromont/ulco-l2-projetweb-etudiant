<?php if (!isset($_SESSION['id'])) : // if user is not connected?>
<nav class="animate__animated animate__slideInDown">
    <div class="nav-wrapper">
        <img src="/public/images/new/logo.png" alt="Logo" style="height: 45%; margin-top: 25px">
        <a href="/">Accueil</a>
        <a href="/store">Magasin</a>
        <a href="/news">Communauté</a>
        <a class="download-button nav-top" href="/download">Installer Staem</a>
        <a href="/account" class="nav-top" style="margin-top: -47px">
            Se connecter à Staem
        </a>
    </div>
</nav>
<?php else : // if user is connected ?>
<nav class="animate__animated animate__slideInDown">
    <div class="nav-wrapper">
        <img src="/public/images/new/logo.png" alt="Logo" style="height: 45%; margin-top: 25px">
        <a href="/">Accueil</a>
        <a href="/store">Magasin</a>
        <a href="/news">Communauté</a>
        <a class="download-button nav-top" href="/download">Installer Staem</a>

        <ul id="nav-dropdown">
            <li>
                <a href="/account/infos" class="nav-top" style="margin-top: 12px">
                    <?= $_SESSION['name'] ?>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" fill="currentColor"
                         class="bi bi-caret-down-fill" viewBox="0 0 16 16">
                        <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1
                   1 0 0 1-1.506 0z"/>
                    </svg>
                </a>
                <ul>
                    <li class="nav-dropdown-item"><a class="nav-top" href="/account/infos">Détails du compte</a></li>
                    <li class="nav-dropdown-item"><a href="/account/cart" class="nav-top">
                            Panier <?= (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0
                                ? "<span class='circled'>".count($_SESSION['cart'])."</span>" : "") ?>
                        </a></li>
                    <li class="nav-dropdown-item">
                        <a class="nav-top" href="/account/logout">
                            Déconnexion
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<?php endif; ?>