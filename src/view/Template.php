<?php

namespace view;

class Template {
  static function render($params) {
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <?php include "commons/head.php" ?>
</head>

<body>

    <?php include "commons/nav.php"; ?>
    <div id="main-wrapper">
        <?php include "modules/".$params["module"]; ?>
    </div>
    <?php include "commons/footer.php"; ?>

</body>

</html>

<?php
  }
}