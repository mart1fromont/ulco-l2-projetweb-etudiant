<?php

namespace controller;

class IndexController {

    /** Renders the home page */
    public function index(): void {
        // Variables à transmettre à la vue
        $params = [
            "title"  => "Accueil | Staem",
            "module" => "home.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    /** Renders the client download page */
    public function download(): void
    {
        // Variables à transmettre à la vue
        $params = [
            "title"  => "Télécharger le client Staem | Staem",
            "module" => "download.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    /** Renders the Staem news page */
    public function news(): void
    {
        // Variables à transmettre à la vue
        $params = [
            "title"  => "Dernières actualités | Staem",
            "module" => "news.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

}