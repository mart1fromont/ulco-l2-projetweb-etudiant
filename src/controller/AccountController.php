<?php
namespace controller;

use model\AccountModel;

class AccountController
{
    /** Renders the account sign-in/sign-up page */
    public static function account(): void {
        // Variables à transmettre à la vue
        $params = array(
            "title" => "Mon compte | Staem",
            "module" => "account.php",
            "status" => ($_GET['status'] ?? "")
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    /** Renders the account infos page */
    public static function infos() {
        // if user is not connected
        if (!isset($_SESSION['id'])){
            AccountController::account();
            return;
        }

        $info = AccountModel::userInfo($_SESSION['id']);

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Informations du compte | Staem",
            "module" => "infos.php",
            "user" => $info,
            "status" => ($_GET['status'] ?? "")
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }


    /** Add the post-specified user to database, if possible */
    public function signin() : void {
        $signinResult = \model\AccountModel::signin($_POST['firstname'], $_POST['lastname'], $_POST['mail'],
            $_POST['password']); // try to add user to database

        // check result, and redirect
        if ($signinResult)
            header("Location: /account?status=signin_success");
        else
            header("Location: /account?status=signin_failed");
    }

    /** Update the specified user in database, if possible */
    public function update() : void {
        // try to update user in database
        $updateResult = \model\AccountModel::update($_SESSION['id'] ?? "", $_POST['firstname'] ?? "",
            $_POST['lastname'] ?? "", $_POST['mail'] ?? "", $_POST['password1'] ?? "");

        // check result, and redirect
        if ($updateResult) {
            header("Location: /account/infos?status=update_success");
        } else
            header("Location: /account/infos?status=update_failed");
    }

    /** Try to login the user, if informations are correct */
    public function login() : void {
        $retrievedAccount = \model\AccountModel::login($_POST['mail']);

        // check retrieved account with current user
        if ($retrievedAccount['mail'] == $_POST['mail']
            && password_verify($_POST['password'], $retrievedAccount['password'])){
            // user is now connected ($_SESSION vars will be encrypted)
            $_SESSION['id'] = $retrievedAccount['id'];
            $_SESSION['name'] = $retrievedAccount['firstname']." " .$retrievedAccount['lastname'];

            header("Location: /store?status=login_success");
        }else{ // if incorrect informations were provided
            header("Location: /account?status=login_failed");
        }
    }

    /** Log out the current connected user */
    public function logout() : void {
        if (isset($_SESSION)) session_destroy();
        header("Location: /account?status=logout");
    }

    /** Sending the account staemworks request and redirect to account infos page */
    public static function staemworksRequest(): void {
        if ($_SERVER['SERVER_NAME'] != "isnvie-test.alwaysdata.net"){
            header("Location: /account/infos?status=no_stw_rqst");
            exit();
        }

        // send request's mail
        $message = "A new user has requested a Staemworks access:\nUser name: ".$_SESSION['name']
            ."\nUser id: ".$_SESSION['id']."\nThis is an auto-generated mail. Do not respond.";
        mail("contact.verifix@gmail.com", "[PROJET WEB L2] New Staemworks request", $message);

        // redirect to account infos page
        header("Location: /account/infos?status=stw_rqst");
    }
}