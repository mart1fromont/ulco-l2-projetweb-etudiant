<?php
namespace controller;

use model\CommentModel;

class CommentController
{
    public static function postComment() {
        $hasCommentBeenAdded = CommentModel::insertComment($_SESSION['id'], $_POST['product-id'],
            $_POST['new-comment-text']);

        // show updated product page
        if ($hasCommentBeenAdded)
            StoreController::product($_POST['product-id'], "post_success");
        else // if comment cannot be added
            StoreController::product($_POST['product-id'], "post_failed");
    }
}