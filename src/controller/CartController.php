<?php
namespace controller;

class CartController
{
    /** Renders the cart page */
    public static function cart(): void
    {
        // if user is not connected
        if (!isset($_SESSION['id'])){
            AccountController::account();
            return;
        }

        $params = array(
            "title" => "Mon panier | Staem",
            "module" => "cart.php"
        );

        \view\Template::render($params);
    }

    /** Add a specific product to the user cart*/
    public static function add(): void
    {
        // if user is not connected
        if (!isset($_SESSION['id'])){
            AccountController::account();
            return;
        }

        if ($_POST['count'] <= 0)
            unset($_SESSION['cart'][$_POST['name']]);
        else{
        $_SESSION['cart'][$_POST['name']]['id'] = $_POST['name'];
        $_SESSION['cart'][$_POST['name']]['count'] = $_POST['count'];
            }

        // redirect to cart page
        $params = array(
            "title" => "Mon panier | Staem",
            "module" => "cart.php",
            "add" => ($_POST['count'] <= 0 ? "drop" : "success")
        );

        \view\Template::render($params);
    }
}