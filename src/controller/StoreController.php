<?php

namespace controller;

use model\CommentModel;
use model\StoreModel;

class StoreController {

    public static function store(): void {
        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();
        $products = \model\StoreModel::listProducts();

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Boutique | Staem",
            "module" => "store.php",
            "categories" => $categories,
            "products" => $products,
            "status" => ($_GET['status'] ?? "")
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    /** Renders the secret product page */
    public static function secret(): void {
        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();
        $products = \model\StoreModel::listProducts();

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Boutique | The Arcade Tower",
            "module" => "tat.php"
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    /** Renders the specified product page
     * @param int $id Product id to show
     */
    public static function product(int $id, string $status = "null") {
        $info = StoreModel::infoProduct($id);
        if (!isset($info[0])){
            StoreController::store();
            exit();
        }

        $comments = CommentModel::listComments($id);
        $params = array(
            "title" => "Acheter ".$info[0]['name']." | Staem",
            "module" => "product.php",
            "product" => $info[0],
            "comments" => $comments,
            "status" => $status
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    /** Search for products in the database */
    public static function search() {
        // Communications avec la base de données
        $categories = \model\StoreModel::listCategories();
        $products = \model\StoreModel::listProducts($_POST['category'] ?? null, $_POST['order'] ?? "none",
            $_POST['search'] ?? "");

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Boutique | Staem",
            "module" => "store.php",
            "categories" => $categories,
            "products" => $products,
            "status" => ($_GET['status'] ?? "")
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

}