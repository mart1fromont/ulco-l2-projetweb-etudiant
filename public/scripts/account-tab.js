/* Account infos javascript functions */
const mailRegex = /\S+@\S+\.\S+/;
const numberRegex = /.*[0-9].*/;
const letterRegex = /.*[A-z].*/;
// user info form
let firstnameInput = [document.getElementById("info-form-firstname"),
    document.getElementById("info-form-firstname-label")];
let lastnameInput = [document.getElementById("info-form-lastname"),
    document.getElementById("info-form-lastname-label")];
let mailInput = [document.getElementById("info-form-mail"),
    document.getElementById("info-form-mail-label")];
let infoFormDiv = document.getElementById("info-form");

// password form
let password1Input = [document.getElementById("password-form-password2"),
    document.getElementById("password-form-password2-label")];
let password2Input = [document.getElementById("password-form-password1"),
    document.getElementById("password-form-password1-label")];
let passwordForm = document.getElementById("password-form");

/** Set array elements to specified value */
function setInputValue(input, value){
    for (let i = 0; i < input.length; ++i){
        if (value === 'invalid'){
            input[i].classList.add("invalid");
            input[i].classList.remove("valid");
        }else if (value === 'valid'){
            input[i].classList.add("valid");
            input[i].classList.remove("invalid");
        }else{
            input[i].classList.remove("valid");
            input[i].classList.remove("invalid");
        }
    }
}

/** Change the selected tab */
function openTab(evt, name) {
    // Declare all variables
    let i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++)
        tabcontent[i].style.display = "none";

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++)
        tablinks[i].className = tablinks[i].className.replace(" active", "");

    document.getElementById(name).style.display = "block";
    evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();

/** Check for user info form */
function checkInfoForm() {
    let pass = true;

    // Check Mail
    if (mailRegex.test(mailInput[0].value.toLowerCase()) && mailInput[0].value.length > 0)
        setInputValue(mailInput, 'valid');
    else if (mailInput[0].value.length > 0){
        setInputValue(mailInput, 'invalid');
        pass = false;
    }else{
        setInputValue(mailInput, 'none');
    }


    // Check lastname
    if (lastnameInput[0].value.length >= 2)
        setInputValue(lastnameInput, 'valid');
    else if (lastnameInput[0].value.length > 0){
        setInputValue(lastnameInput, 'invalid');
        pass = false;
    }else{
        setInputValue(lastnameInput, 'none');
    }


    // Check lastname
    if (firstnameInput[0].value.length >= 2)
        setInputValue(firstnameInput, 'valid');
    else if (firstnameInput[0].value.length > 0){
        setInputValue(firstnameInput, 'invalid');
        pass = false;
    }else{
        setInputValue(firstnameInput, 'none');
    }

    return pass;
}

function checkPasswordForm(){
    if (password1Input[0].value.trim().length > 5 && password2Input[0].value.trim().length > 5
        && password1Input[0].value.localeCompare(password2Input[0].value) === 0
        && numberRegex.test(password1Input[0].value.trim())
        && letterRegex.test(password1Input[0].value.trim())){
        setInputValue(password1Input, 'valid');
        setInputValue(password2Input, 'valid');
        return true;
    }else{
        setInputValue(password1Input, 'invalid');
        setInputValue(password2Input, 'invalid');
        return false;
    }
}


// Submit and change events
infoFormDiv.onchange = function() {checkInfoForm()};
infoFormDiv.onsubmit = function(e) {
    if (!checkInfoForm())
        e.preventDefault();
};
passwordForm.onchange = function() {checkPasswordForm()};
passwordForm.onsubmit = function(e) {
    if (!checkPasswordForm())
        e.preventDefault();
};

