/* Cart page javascript */

/** Change the current product count from 1 to 5
 * @param {number} value 1 to add or -1 to remove one item
 * @param {number} id the id of the product
 */
function changeProductCount(id, value){
    let count = document.getElementById("product-select-count-" + id);
    let countInput = document.getElementById("product-select-count-input-" + id);

    console.log(parseInt(countInput.value) + value)
    if (parseInt(countInput.value) + value > 5 || parseInt(countInput.value) + value < 0)
        return;

    countInput.value = parseInt(countInput.value) + value;
    count.value = countInput
    count.innerText = countInput.value;
    document.getElementById("form-" + id).submit();
}
