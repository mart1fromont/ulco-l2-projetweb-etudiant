/* Signin javascript functions */
const mailRegex = /\S+@\S+\.\S+/;
const numberRegex = /.*[0-9].*/;
const letterRegex = /.*[A-z].*/;
let password1Input = [document.getElementById("password2"), document.getElementById("password2-label")];
let password2Input = [document.getElementById("password1"), document.getElementById("password1-label")];
let firstnameInput = [document.getElementById("firstname"), document.getElementById("firstname-label")];
let lastnameInput = [document.getElementById("lastname"), document.getElementById("lastname-label")];
let mailInput = [document.getElementById("mail"), document.getElementById("mail-label")];
let formDiv = document.getElementById("signin-form");

/** Set array elements to specified value */
function setInputValue(input, value){
    for (let i = 0; i < input.length; ++i){
        if (value === 'invalid'){
            input[i].classList.add("invalid");
            input[i].classList.remove("valid");
        }else{
            input[i].classList.add("valid");
            input[i].classList.remove("invalid");
        }
    }
}

/** Check for user form */
function checkForm() {
    let pass = true;

    // Check Passwords
    if (password1Input[0].value.trim().length > 5 && password2Input[0].value.trim().length > 5
        && password1Input[0].value.localeCompare(password2Input[0].value) === 0
        && numberRegex.test(password1Input[0].value.trim())
        && letterRegex.test(password1Input[0].value.trim())){
        setInputValue(password1Input, 'valid');
        setInputValue(password2Input, 'valid');
    }else{
        setInputValue(password1Input, 'invalid');
        setInputValue(password2Input, 'invalid');
        pass = false;
    }

    // Check Mail
    if (mailRegex.test(mailInput[0].value.toLowerCase()) && mailInput[0].value.length !== 0)
        setInputValue(mailInput, 'valid');
    else{
        setInputValue(mailInput, 'invalid');
        pass = false;
    }

    // Check lastname
    if (lastnameInput[0].value.length >= 2)
        setInputValue(lastnameInput, 'valid');
    else{
        setInputValue(lastnameInput, 'invalid');
        pass = false;
    }

    // Check firstname
    if (firstnameInput[0].value.length >= 2)
        setInputValue(firstnameInput, 'valid');
    else{
        setInputValue(firstnameInput, 'invalid');
        pass = false;
    }

    return pass;
}

// Submit and change events
formDiv.onchange = function() {checkForm()};
formDiv.onsubmit = function(e) {
    if (!checkForm())
        e.preventDefault();
};