/* Product javascript functions */
// Variables
let countInput = document.getElementById("product-select-count");
let errorBox = document.getElementById("product-select-error");
let imageBox = document.getElementById("product-showcase-main");
let countFormInput = document.getElementById("product-select-count-input");
let imagesList = document.querySelectorAll(".product-image");
let itemCount = 1;

/** Change the current product count from 1 to 5
 * @param {number} value 1 to add or -1 to remove one item
 */
function changeProductCount(value){
    if (itemCount + value > 5 || itemCount + value < 1)
        return;

    itemCount += value;
    countFormInput.value = itemCount;
    countInput.value = itemCount;
    countInput.innerText = itemCount.toString();
    errorBox.style.opacity = "0"; // will be set to 'visible' if error

    if (itemCount >= 5) { // max count
        errorBox.style.opacity = "1";
        errorBox.innerText = 'Quantité maximale autorisée !';
    }
}

/** onLoad */
imagesList.forEach(function(entry){
    entry.onclick = function() {
        imageBox.src = entry.src;
    };
});

/** show the small add to cart div when scrolling*/
window.onscroll = function() {
    document.getElementById('product-infos-small').className =
        window.scrollY > 450 ? 'product-infos-small visible' : 'product-infos-small hidden';
};


errorBox.style.opacity = '0';

document.getElementById("product-select-add").onclick = function () {changeProductCount(1)};
document.getElementById("product-select-remove").onclick = function () {changeProductCount(-1)};