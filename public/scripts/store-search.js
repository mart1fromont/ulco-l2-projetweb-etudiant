/* Store search javascript functions */
/** Reset choices in the form (should work better than just a type=reset button) */
function resetForm(e) {
    e.preventDefault();

    // [obsolete] Order checkboxes
    /* document.getElementById('order-asc-checkbox').checked = false;
    document.getElementById('order-desc-checkbox').checked = false; */

    // Categories checkboxes
    let elts = document.getElementsByClassName("category-checkbox");
    for (let elt in elts) {
        elts[elt].checked = undefined;
    }
}
