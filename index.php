<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

// Start PHP session
session_start();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');
$router->get('/download', 'controller\IndexController@download');
$router->get('/news', 'controller\IndexController@news');

$router->get('/store', 'controller\StoreController@store');
$router->get('/store/42', 'controller\StoreController@secret');

$router->get('/account', 'controller\AccountController@account');
$router->get('/account/logout', 'controller\AccountController@logout');
$router->get('/account/infos', 'controller\AccountController@infos');

$router->get('/account/cart', 'controller\CartController@cart');
$router->get('/account/staemworks-request', 'controller\AccountController@staemworksRequest');


// POST
$router->post("/account/login", "controller\AccountController@login");
$router->post("/account/signin", "controller\AccountController@signin");
$router->post("/account/update", "controller\AccountController@update");

$router->post("/store/postComment", "controller\CommentController@postComment");
$router->post("/store/search", "controller\StoreController@search");

$router->post("/cart/add", "controller\CartController@add");


// Store pages
$router->get('/store/{:num}', function($id) {controller\StoreController::product($id);});


// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/
$router->listen();
